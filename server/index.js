var express = require('express');
var app = express();
var api = require('./api');
var config = require('./config');
var err = require('./middleware/err');

require('mongoose').connect(config.db.url);

if (config.seed) {
    require('./util/seed');
}

// setup the app middleware
require('./middleware')(app);

// setup the api
app.use('/api', api);

// setup global error handling
app.use(err());

// export the app for testing
module.exports = app;
