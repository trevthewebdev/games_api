var _ = require('lodash');

exports.parseOptions = function (opts) {
    var arr = _.replace(opts, ' ', '').split(',');
    var obj = { fields: {} };

    for (var i = 0; i < arr.length; i++) {
        obj.fields[arr[i]] = 1;
    };

    return obj;
};

exports.mergeOptions = function (defaults, options) {
    var merged = (options) ? _.merge({}, defaults, this.parseOptions(options)) : defaults;
    if (merged.id) _.uset(merged, 'id');

    return merged;
};

exports.duplicateFound = function (doc, message) {
    res.status(409).json({
        'message': message || 'Already Exists',
        'document': doc
    });
};