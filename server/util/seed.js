var User = require('../api/user/userModel');
var Game = require('../api/game/gameModel');
var _ = require('lodash');

console.log('\n---| START SEEDING |---');

/* Seed Base Data
   ========================================================================== */

  var users = [
    {
      local: {
        email: "kev@gmail.com",
        password: "temp12345"
      },
      profile: {
        firstName: "Kevin",
        lastName: "Williams",
        phone: "555-679-5555"
      },
      permissions: 5,
    },
    {
      local: {
        email: "josh@gmail.com",
      },
      profile: {
        firstName: "Josh",
        lastName: "Simone",
        phone: "555-679-3746"
      },
      permissions: 2,
    }
  ];

  var stats = [
    {
      scoring: [{score: 105, place: 2}],
      duration: 1.5
    },
    {
      scoring: [{score: 55, place: 1}],
      duration: 2
    }
  ];

  var games = [
    {
      "name": "Battlelore Second Edition",
      "desc": {
        "duration": 1.5,
        "playerNumMin": 2,
        "playerNumMax": 2,
        "playerNumBest": 2,
        "rank": 1,
        "text": "Battlelore is a war style game in which players take turns creating, deploying, and then battling their armies.",
        "theme": "Fantasy",
        "weight": "Medium"
      },
      "media": {
        "images": [
          "path/to/image1.jpg",
          "path/to/image2.jpg"
        ]
      }
    },

    {
      "name": "The Bloddy Inn",
      "desc": {
        "duration": 0.5,
        "playerNumMin": 1,
        "playerNumMax": 4,
        "playerNumBest": 3,
        "rank": 2,
        "text": "Bloody Inn is a card game where you take on the roll of family member trying to run an inn and be the most wealthy family member by the end of the season.",
        "weight": "Light"
      },
      "media": {
        "images": [
          "path/to/image1.jpg",
          "path/to/image2.jpg",
          "path/to/image3.jpg",
          "path/to/image4.jpg"
        ]
      }
    }
  ];


/**
 * Create Doc
 */
var createDoc = function(model, doc) {
  return new Promise(function(resolve, reject) {
    new model(doc).save(function(err, saved) {
      return err ? reject(err) : resolve(saved);
    });
  });
};


/**
 * Drop Database
 */
var cleanDB = function() {
  console.log('...dropping db\n');
  var cleanPromises = [Game, User]
    .map(function(model) {
      return model.remove().exec();
    });
  return Promise.all(cleanPromises);
};


/**
 * Users
 */
var createUsers = function (data) {
  console.log('\n...loading in users');
  var promises = users.map(function(user) {
    return createDoc(User, user);
  });

  return Promise.all(promises)
    .then(function (users) {
      return _.merge({users: users}, data || {});
    });
};

/**
 * Games
 */
var createGames = function(data) {
  console.log('...loading in games');

  var addGame = function (game) {
    console.log("\n--- " + game.name + " ---");
    console.log(game._id);
    User.update(
      {_id: data.users[0]},
      {$push: {'content.games': game._id}},
      function (err, user) {
        if (err) console.log(err)
      }
    );
  };

  var promises = games.map(function(game) {
    return createDoc(Game, game);
  });

  return Promise.all(promises)
    .then(function(games) {
      var mod = games.map(function (game, i) {
        addGame(game);
      });
      return _.merge({games: mod}, data || {});
    })
    .then(function() {
      return 'Seeded DB with 3 Games';
    });
};

cleanDB()
  .then(createUsers)
  .then(createGames)
  .catch(function (err) {
    console.log("\n---| OOPS |---");
    console.log(err);
  });
