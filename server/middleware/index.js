var morgan = require('morgan');
var bodyParser = require('body-parser');
var errors = require('./err');
var coors = require('./coors');

module.exports = function(app) {
    app.use(morgan('dev'));
    app.use(coors);
    app.use(bodyParser.urlencoded({ extend: true }));
    app.use(bodyParser.json());
};
