var router = require('express').Router();

// note: paths start with /api
// router.use('/admin', require('./admin'));
router.use('/players', require('./user'));
router.use('/games', require('./game'));
router.use('/events', require('./event'));

module.exports = router;