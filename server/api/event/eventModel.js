var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Event = new Schema({

    isPrivate: {
        type: Boolean,
        default: false
    },

    date: Date,
    starts: Date,
    ends: Date,
    venue: {
        name: String,
        address1: String,
        address2: String
    },

    playerCount: Number,
    maxPlayerCount: Number,
    maxVotes: {
        type: Number,
        default: 2
    },

    organizers: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],

    players: [{
        player: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        games: [{
            type: Schema.Types.ObjectId,
            ref: 'Game'
        }]
    }]
});

module.exports = mongoose.model('Event', Event);