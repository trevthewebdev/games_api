var _ = require('lodash');
var async = require('async');
var Event = require('./eventModel');

// All Events
exports.get = function (req, res, next) {
    Event.find(function (err, event) {
        if (err) return next(err);
        res.json(event);
    });
};

exports.post = function (req, res, next) {
    var newEvent = req.body;

    /*
        Probably would be good to check if an event already
        exists on a certian date.

        IF event dates are the same AND the duplicate isn't a
        private event.

        THEN ask if they want to continue saving.
    */

    Event.create(newEvent, function (err, event) {
        if (err) res.json({
            'success': false,
            'message': 'Error Saving to the Database: ' + err
        });
        res.json({
            'success': true,
            'message': 'Successfully saved event.',
            'document': event
        });
    });
};

// Single Event
exports.getOne = function (req, res, next) {
    Event.findOne(
        { _id: req.params.id },
        function (err, event) {
            if (err) return next(err);
            res.json(event);
        });
};

exports.put = function (req, res, next) {
    console.log("made it");
    var query = { _id: req.params.id };
    var update = req.body;
    var options = req.body.options;

    function findEvent (callback) {
        Event.findOne({}, function (err, event) {
            callback(null, event);
        });
    }

    function saveEvent (event, callback) {
        console.log("event: ", event);
        return callback(null, event);
    }

    async.waterfall([
        findEvent,
        saveEvent,
        function (err, event) {
            if (err) console.log(err);
            console.log(event);
        }
    ]);

    // Event.findOneAndUpdate(query, update, options,
    //     function (err, event) {
    //        if (err) res.json({
    //            'success': false,
    //            'message': 'Error Updating Game: ' + err
    //        });

    //        res.json({
    //            'success': true,
    //            'message': 'Event successfully updated.',
    //            'document': event
    //        });
    //     });
};

exports.delete = function (req, res, next) {

};
