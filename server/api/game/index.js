var router = require('express').Router();
var controller = require('./gameController');

// All Games
router.get('/', controller.get);
router.post('/', controller.post);

// Single Game
router.get('/:id', controller.getOne);
router.put('/:id', controller.put);
router.delete('/:id', controller.delete);

module.exports = router;