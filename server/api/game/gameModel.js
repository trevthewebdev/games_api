var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Game = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },

    desc: {
        category: [{
            type: Schema.Types.ObjectId,
            ref: 'Category'
        }],
        duration: Number,
        edition: Number,
        playerNumMin: Number,
        playerNumMax: Number,
        playerNumBest: Number,
        rank: Number,
        text: String,
        theme: String,
        weight: String
    },

    media: {
        images: [ String ],
        videos: [ String ],
        downloads: [ String ]
    },

    rules: {
        type: Schema.Types.ObjectId,
        ref: 'Rule'
    },

    stats: {
        recentGames:[{
            type: Schema.Types.ObjectId,
            ref: 'Stat'
        }],
        lastWinner: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        mostWins: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        bestScore: {
            player: {
                type: Schema.Types.ObjectId,
                ref: 'Stat'
            },
            score: Number
        }
    }
});

module.exports = mongoose.model('Game', Game);