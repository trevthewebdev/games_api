var Game = require('./gameModel');
var _ = require('lodash');
var helpers = require('../../util/requestHelpers');

// All Games
exports.get = function (req, res, next) {
    Game.find(function (err, game) {
        if (err) return next(err);
        res.json(game);
    });
};

exports.post = function (req, res, next) {
    Game.findOne(
        { name: req.body.name },
        function (err, game) {
            if (err) return next(err);
            if (game) helpers.duplicateFound(res, {'id': game._id});

            var newGame = req.body;
            Game.create(newGame, function (err, game) {
                if (err) res.json({
                    'success': false,
                    'message': 'Error Saving to the Database',
                    'err': err
                });
                res.json({
                    'success': true,
                    'message': 'Game successfully saved',
                    'document': game
                });
            });
    });
};

// Single Game
exports.getOne = function (req, res, next) {
    var query = { _id: req.params.id };

    Game.findOne(query, function (err, game) {
            if (err) return next(err);
            res.json(game);
        });
};

exports.put = function (req, res, next) {
    var query = { _id: req.params.id };
    var update = req.body;
    var options = {
        new: true,
        fields: { _id: 1, name: 1 }
    };
    var options = helpers.mergeOptions(options, req.body.options);

    Game.findOneAndUpdate(query, update, options,
        function (err, game) {
            if (err) res.json({
                'success': false,
                'message': 'Error Updating Game',
                'err': err
            });

            res.json({
                'success': true,
                'message': 'Game Successfully updated',
                'document': game
            });
        });
};

exports.delete = function (req, res, next) {
    var query = { _id: req.params.id };

    Game.findOneAndRemove( query,
        function (err, game) {
            if (err) res.json({
                'success': false,
                'message': 'Error Removing Game',
                'err': err
            });

            res.json({
                'success': true,
                'message': 'Game successfully removed'
            })
        });
};
