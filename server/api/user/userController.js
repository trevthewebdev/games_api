var User = require('./userModel');

// All Members
exports.get = function (req, res, next) {
    User.find()
        .select('-local.password')
        .exec(function (err, user) {
            if (err) return next(err);
            res.json(user);
        });
};

// exports.post = function (req, res, next) {

// };

// // Single Member
// exports.getOne = function (req, res, next) {

// };

// exports.put = function (req, res, next) {

// };

// exports.delete = function (req, res, next) {

// };