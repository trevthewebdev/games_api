var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
    local: {
        email: String,
        password: String
    },

    facebook: {
        id: String,
        token: String,
        email: String
    },

    profile: {
        firstName: String,
        lastName: String,
        phone: String,
        image: {
            type: String,
            default: '/images/profile-default--small.png'
        }
    },

    content: {
        games: [{
            type: Schema.Types.ObjectId,
            ref: 'Game'
        }],
        favorites: [{
            type: Schema.Types.ObjectId,
            ref: 'Game'
        }],
        images: [ String ]
    },

    stats: {
        games: {
            played: {
                type: Number,
                default: 0
            },
            won: {
                type: Number,
                default: 0
            },
            lost: {
                type: Number,
                default: 0
            },
            list: [{
                name: String,
                wins: Number,
                loses: Number
            }]
        }
    },

    permissions: {
        type: Number,
        default: 1
    },

    passwordReset: {
        token: String,
        expires: Date
    }
});

User.methods.hashPassword = function ( password ) {
    return bcrypt.hashSync( password, bcrypt.genSaltSync() );
};

User.methods.validatePassword = function ( password ) {
    return bcrypt.compareSync( password, this.local.password );
};

module.exports = mongoose.model('User', User);
