var router = require('express').Router();
var controller = require('./userController');

// All Games
router.get('/', controller.get);
// router.post('/:id', controller.post);

// Single Game
// router.get('/:id', controller.getOne);
// router.put('/:id', controller.put);
// router.delete('/:id', controller.delete);

module.exports = router;