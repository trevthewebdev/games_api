/* ==========================================================================
   Description:
   Stats are children of and tied to a GAME
   ========================================================================== */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Stat = new Schema({
    winner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    scoring: [{
        player: {
          type: Schema.Types.ObjectId,
          ref: 'User'
        },
        score: Number,
        place: Number
    }],
    duration: Number
});

module.exports = mongoose.model('Stat', Stat);