// setup config first before anything by requiring it
var config = require('./server/config');
var app = require('./server');

app.listen(config.port);
console.log('listening on http://localhost: ' + config.port);
